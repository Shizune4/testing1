
import React, { useState, useEffect } from "react";


const App = props => {
  const [users, setUsers] = useState([]);

  let nameRef = React.createRef();
  useEffect(() => {
    fetch('http://192.168.0.102:8000/api/todolist')
      .then(res => res.json())
      .then(json => {
        setUsers(json.data);
      });
  }, []);

  // const somethingadd = () => {
  //   fetch('http://192.168.0.102:8000/api/todolist', {
  //     method: 'POST',
  //     headers: {
  //       'content-type': 'application/json'
  //     },
  //     body: JSON.stringify({ id: 6, name: 'Tom', valueHolder: 'false' })
  //   })
  //     .then(res => res.json())
  //     .then(tom => {
  //       setUsers([...users, tom]);
  //     });
  // }
  const apiadd = () => {
    let id = users.length + 1;
    let name = nameRef.current.value;

    fetch('http://192.168.0.102:8000/api/todolist', {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify({ id: id, name: name, valueHolder: 'false' })
    })
      .then(res => res.json())
      .then(json => {
        setUsers([...users, json.data])
      });
  }
  const deleteusers = (_id) => {
    fetch(`http://192.168.0.102:8000/api/todolist/${_id}`, {
      method: 'DELETE',
      headers: {
        'content-type': 'application/json'
      }
    })
    let del = users.filter(i => i._id !== _id)
    setUsers(del)
  }


  return (
    <div>
      <input type='text' ref={nameRef} />
      <ul>
        {users.map(u => <li key={u.id}>{u.id},{u.name}<button onClick={() => deleteusers(u._id)}>Delete</button></li>)}

      </ul>
      <button onClick={apiadd}>New User</button>
    </div>
  );
}
export default App;



